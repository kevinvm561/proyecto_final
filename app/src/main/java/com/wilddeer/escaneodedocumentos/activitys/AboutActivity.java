package com.wilddeer.escaneodedocumentos.activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.wilddeer.escaneodedocumentos.R;
import com.wilddeer.escaneodedocumentos.adapters.TeamAdapter;

public class AboutActivity extends AppCompatActivity {

    private RecyclerView listTeam;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        initToolbar();
        listTeam = findViewById(R.id.listTeam);
        listTeam.setLayoutManager(new GridLayoutManager(this, 3));
        listTeam.setHasFixedSize(true);
        TeamAdapter adapter = new TeamAdapter(this);
        listTeam.setAdapter(adapter);
    }
    private void initToolbar() {
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Acerca de");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
