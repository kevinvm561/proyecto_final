package com.wilddeer.escaneodedocumentos.activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.wilddeer.escaneodedocumentos.BuildConfig;
import com.wilddeer.escaneodedocumentos.R;
import com.wilddeer.escaneodedocumentos.adapters.DocAdapter;
import com.wilddeer.escaneodedocumentos.adapters.DocumentAdapter;
import com.wilddeer.escaneodedocumentos.classes.Alumnos;
import com.wilddeer.escaneodedocumentos.classes.Documento;
import com.wilddeer.escaneodedocumentos.classes.ServerURL;
import com.wilddeer.escaneodedocumentos.classes.TipoDoc;
import com.wilddeer.escaneodedocumentos.utils.AlertDialogCustom;
import com.wilddeer.escaneodedocumentos.utils.CreatePDFDocument;
import com.wilddeer.escaneodedocumentos.utils.FileUtils;
import com.wilddeer.escaneodedocumentos.utils.LoaderList;
import com.wilddeer.escaneodedocumentos.utils.NamesFormat;
import com.wilddeer.escaneodedocumentos.utils.UploadDocument;
import com.wilddeer.escaneodedocumentos.utils.ViewAnimation;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;



public class PerfilAlumnoActivity extends AppCompatActivity {
    private String currentPhotoPath;
    private Alumnos alumno;
    private RequestQueue queue;
    private byte fistInit = 0;
    private RecyclerView listDocuments;
    private FloatingActionButton btnEscaner;
    private DocumentAdapter adapter;
    private TextView lbNombre;
    private TextView lbGrupo;
    private TextView lbCarrera;
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private String[] documents = new String[]
            {"Acta de Nacimiento", "Curp", "Certificado de Preparatoria", "Ficha de Pago"};
    private TipoDoc SELECTED_DOCUMENT;
    private CircularImageView mImagenPerfil;
    private LinearLayout lyt_progress;
    private ArrayList<TipoDoc> docs;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_alumno);
        adapter = new DocumentAdapter(this, new ArrayList<>());
        queue = Volley.newRequestQueue(this);
        alumno = (Alumnos) getIntent().getExtras().getSerializable(getString(R.string.alumno_index));

        initToolbar();
        init();


    }


    private void init() {
        listDocuments = findViewById(R.id.listDocuments);
        btnEscaner = findViewById(R.id.btnEscaner);
        lbNombre = findViewById(R.id.lbNombre);
        //lbGrupo = findViewById(R.id.lbGrupo);
        lbCarrera = findViewById(R.id.lbCarrera);
        mImagenPerfil = findViewById(R.id.imagenPerfil);
        lyt_progress = findViewById(R.id.lyt_progress);
        listDocuments.setLayoutManager(new LinearLayoutManager(this));
        listDocuments.setHasFixedSize(true);
        listDocuments.setAdapter(adapter);
        findViewById(R.id.lyt_back).setOnClickListener(clicl -> finish());
        consultDocuments(true);
        lbCarrera.setText(alumno.getCarrera());
//        lbGrupo.setText(alumno.getGrupo());
        lbNombre.setText(NamesFormat.formatNombre(alumno.getNombre(), alumno.getApellidoPaterno()));
        Picasso.get().load(alumno.getUrlImageFoto()).into(mImagenPerfil);
        btnEscaner.setOnClickListener(this::btnEscanerAction);

    }

    private void btnEscanerAction(View view) {
        AlertDialogCustom.showDialogChoise(
                this,
                "Elije el tipo de documento",
                createListDocAlert(),
                (dialog, which) -> {
                    SELECTED_DOCUMENT = docs.get(which);
                    dialog.dismiss();
                    dispatchTakePictureIntent();
                });
    }

    @NotNull
    private String[] createListDocAlert() {
        String[] list = new String[docs.size()];
        for (int x = 0; x < docs.size(); x++)
            list[x] = docs.get(x).getDoc();
        return list;
    }


    private void consultDocuments(boolean first) {

        if(first)
            LoaderList.showLoading(lyt_progress, listDocuments);
        else
            LoaderList.showLoading(lyt_progress, listDocuments, findViewById(R.id.lyt_empty));
        StringRequest request = new StringRequest(
                Request.Method.POST, ServerURL.CONSULT_DOCUMENTS,
                this::loadDocuments,
                r -> Log.e("MSG", r.toString())) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idalumno", String.valueOf(alumno.getId()));
                return params;
            }
        };
        queue.add(request);

    }

    private void loadDocuments(String response) {
        if(!NamesFormat.IsJsonString(response))
        {
            Log.e("MSG", response);
            AlertDialogCustom.showMensaggeDialog(this, "Error", "Se produjo un error en el servidor", (click, pos) -> finish());
            LoaderList.showEmpty(lyt_progress, findViewById(R.id.lyt_empty));
            return;
        }

        try {
            ArrayList<Documento> list = new ArrayList<>();
            JSONObject object = new JSONObject(response);
            JSONArray array = object.getJSONArray("documentos");
            docs = loadTipoDoc(object.getJSONArray("tipo_doc"));
            if (array.length() > 0) {
                for (int x = 0; x < array.length(); x++)
                    list.add(new Documento(array.getJSONObject(x)));

                adapter.setItems(list);
                adapter.notifyDataSetChanged();

                LoaderList.showList(lyt_progress, listDocuments);
            } else {
                LoaderList.showEmpty(lyt_progress, findViewById(R.id.lyt_empty));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("ERROR", e.getMessage());

        }
    }

    private ArrayList<TipoDoc>  loadTipoDoc(JSONArray array)
    {
        ArrayList<TipoDoc> tipoDocs = new ArrayList<>();
        try {
            for (int x = 0; x < array.length(); x++)
                tipoDocs.add(new TipoDoc(array.getJSONObject(x)));
        }catch (JSONException ex) {
            ex.printStackTrace();
        }
        return tipoDocs;


    }


    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                Log.e("MDG", "Imagen Guardada");
                CreatePDFDocument create = new CreatePDFDocument(this, currentPhotoPath,
                        alumno, SELECTED_DOCUMENT.getDoc(), this::finishCreateDocument);
                create.execute(currentPhotoPath);
            } else {
                Log.e("MSG", "Se cancelo");
                FileUtils.deleteFile(currentPhotoPath);
            }

        }
    }

    private void finishCreateDocument(String res) {
        Documento documento = adapter.isSimiliarDocument(SELECTED_DOCUMENT.getId());
        if(documento != null){
            AlertDialogCustom.showMensaggeDialog(this, getString(R.string.advertencia),
                    getString(R.string.mismo_documento), (click, pos) ->
                    {responseUploadDocument(res,  documento); click.dismiss();});
        }
        else
        {
            responseUploadDocument(res, null);
        }

    }

    private void responseUploadDocument(String res,  Documento doc)
    {

        UploadDocument document = new UploadDocument(res, this, alumno, SELECTED_DOCUMENT);
        if(doc != null)
            document.setDocumento(doc);
        document.uploadDoc(
                response -> {
                    Log.e("MSG", new String(response.data));
                    try {
                        JSONObject object = new JSONObject(new String(response.data));
                        if(object.getBoolean("status"))
                        {
                            consultDocuments(false);
                            AlertDialogCustom.showMensaggeDialog(this, "Documento",
                                    "El documento se a registrado correctamente");
                            document.deleteDocument();

                        }
                        else{

                            AlertDialogCustom.showMensaggeDialog(this, "Error",
                                    "No se registro el documento correctamente");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    document.closeDialog();
                }, error -> {
                    Log.e("MSG", error.getMessage());
                    document.closeDialog();
                });
    }

    private void finishOpenDocument(String res)
    {
        openPDF(res);
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }

        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }
    private void openPDF(String path)
    {
        Intent intent = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            File file=new File(path);
            Uri uri = FileProvider.getUriForFile(this, getPackageName() + ".provider", file);
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(uri);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        } else {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(path), "application/pdf");
            intent = Intent.createChooser(intent, "Open File");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(R.menu.menu_perfil_alumno, paramMenu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        switch (paramMenuItem.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
            case R.id.btnVer:
                break;
            case R.id.btnActualizar:
                consultDocuments(false);
                break;
            case R.id.btnEscaner:
                btnEscanerAction(null);
                break;

        }
        return super.onOptionsItemSelected(paramMenuItem);
    }




}
