package com.wilddeer.escaneodedocumentos.activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wilddeer.escaneodedocumentos.BuildConfig;
import com.wilddeer.escaneodedocumentos.R;
import com.wilddeer.escaneodedocumentos.classes.Usuario;
import com.wilddeer.escaneodedocumentos.database.DBConfig;
import com.wilddeer.escaneodedocumentos.database.DBUsuarios;
import com.wilddeer.escaneodedocumentos.utils.AlertDialogCustom;

import java.io.File;

public class ConfigActivity extends AppCompatActivity {

    private TextView lbNombre;
    private TextView lbUsuario;
    private TextView lbCorreo;
    private LinearLayout btnCerrar;
    private LinearLayout btnEliminar;
    private SwitchCompat cbTema;
    private TextView lbVersion;
    private DBConfig config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        config = new DBConfig(this);
        initToolbar();
        init();
    }

    private Usuario loadUsuario()
    {
        DBUsuarios dbUsuarios = new DBUsuarios(this);
        Usuario usuario = dbUsuarios.consultUsuario();
        return usuario;
    }

    private void setConfig()
    {
        cbTema.setChecked(config.getConfig().getThemeOption() == 0);
    }

    private void init()
    {
        lbNombre = findViewById(R.id.lbNombre);
        lbUsuario = findViewById(R.id.lbUsuario);
        lbCorreo = findViewById(R.id.lbCorreo);
        btnCerrar = findViewById(R.id.btnCerrar);
        btnEliminar = findViewById(R.id.btnEliminar);
        cbTema = findViewById(R.id.cbTema);
        lbVersion = findViewById(R.id.lbVersion);
        Usuario usuario = this.loadUsuario();
        lbNombre.setText(usuario.getNombre());
        lbUsuario.setText(usuario.getUser());
        lbCorreo.setText(usuario.getCorreo());

        lbVersion.setText(String.format("Build Version %s", BuildConfig.VERSION_NAME));
        findViewById(R.id.btnChangeTheme).setOnClickListener(click -> {
            if(cbTema.isChecked())
                cbTema.setChecked(false);
            else
                cbTema.setChecked(true);
        });
        cbTema.setOnCheckedChangeListener((buttonView, isChecked) -> {

            config.setThemeOption(isChecked ? 0 : 1);
            if(isChecked)
            {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
            else{
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }

        });
        btnCerrar.setOnClickListener(this::btnCerrarAction);

        btnEliminar.setOnClickListener(this::btnBorrarAction);
        btnEliminar.setVisibility(View.GONE);
        setConfig();
    }

    private void btnBorrarAction(View view) {
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        String[] nameFiles = storageDir.list();
        for (String name: nameFiles)
        {
            Log.e("FILE", name);
        }
        storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        nameFiles = storageDir.list();
        for (String name: nameFiles)
        {
            Log.e("FILE", name);
        }
    }

    private void btnCerrarAction(View view)
    {
        AlertDialogCustom.showMensaggeDialogDecision(this, "Advertencia",
                "¿Deseas cerrar la session?", ((dialog, which) -> closeSesion()));
    }

    private void closeSesion()
    {
        DBUsuarios db = new DBUsuarios(this);
        db.deleteUser();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void initToolbar() {
        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        if (paramMenuItem.getItemId() == 16908332) {
            finish();
        }
        return super.onOptionsItemSelected(paramMenuItem);
    }
}
