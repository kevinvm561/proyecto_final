/*
 * Copyright (c) 2020. Carlos Santiago, no usar con fines comerciales, solo con fines educativos
 */

package com.wilddeer.escaneodedocumentos.activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.wilddeer.escaneodedocumentos.R;
import com.wilddeer.escaneodedocumentos.classes.ServerURL;
import com.wilddeer.escaneodedocumentos.classes.Usuario;
import com.wilddeer.escaneodedocumentos.database.DBUsuarios;
import com.wilddeer.escaneodedocumentos.utils.AlertDialogCustom;
import com.wilddeer.escaneodedocumentos.utils.Device;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private TextInputEditText txtUsuario;
    private TextInputEditText txtContraseña;
    private FloatingActionButton btnLogin;
    private ProgressBar progressBar;
    private LinearLayout signUpForAccount;
    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();

    }

    private void init()
    {
        txtUsuario = findViewById(R.id.txtUsuario);
        txtContraseña = findViewById(R.id.txtContraseña);
        btnLogin = findViewById(R.id.btnLogin);
        progressBar = findViewById(R.id.progress_bar);
        signUpForAccount = findViewById(R.id.sign_up_for_account);
        queue = Volley.newRequestQueue(this);
        btnLogin.setOnClickListener(this::btnLoginAction);

    }
    private void btnLoginAction(View view) {

        if(!Device.isInternetConnection(this))
        {
            AlertDialogCustom.showMensaggeDialog(this, "Error", getString(R.string.conexion_internet));
            return;
        }
        if (txtContraseña.getText().toString().matches("") || txtUsuario.getText().toString().matches("")) {
            AlertDialogCustom.showMensaggeDialog(this, "Error", getString(R.string.campos_vacios));
            return;
        }
        hideButton();

        StringRequest request = new StringRequest(Request.Method.POST, ServerURL.LOGIN_ACTION,
                this::loginListener,
                error -> {
                    Log.e("ERROR", error.getMessage());
                    AlertDialogCustom.showMensaggeDialog(this, "Error", getString(R.string.errorServer));
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user", txtUsuario.getText().toString());
                params.put("pass", txtContraseña.getText().toString());
                return params;
            }
        };
        queue.add(request);

    }

    private void hideButton()
    {
        this.progressBar.setVisibility(View.VISIBLE);
        this.btnLogin.setAlpha(0.0F);
    }

    private void showButton()
    {
        this.progressBar.setVisibility(View.GONE);
        this.btnLogin.setAlpha(1.0F);
    }

    private void loginListener(String s) {

        try {
            JSONObject object = new JSONObject(s);
            if(!object.getBoolean("status"))
            {
                AlertDialogCustom.showMensaggeDialog(this, "Error", "Usuario o contraseña incorrectos");
            }else
            {
                Usuario usuario = new Usuario(object.getJSONObject("data"));
                DBUsuarios db = new DBUsuarios(this);
                db.openDatabase();
                Log.e("MSG", String.valueOf(db.insertUsuario(usuario)));
                db.close();

                AlertDialogCustom.showMensaggeDialog(this, "Iniciar sesion",
                        String.format("Bienvenido %s", usuario.getNombre()), this::openMenu);

            }

            showButton();
        }catch (JSONException ex)
        {
            ex.printStackTrace();
            showButton();
            AlertDialogCustom.showMensaggeDialog(this, "Error", getString(R.string.errorServer));

        }

    }

    private void openMenu(DialogInterface dialogInterface, int i) {
        if(dialogInterface != null)
            dialogInterface.dismiss();
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }


}
