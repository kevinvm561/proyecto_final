package com.wilddeer.escaneodedocumentos.activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wilddeer.escaneodedocumentos.R;
import com.wilddeer.escaneodedocumentos.adapters.AlumnosAdapter;
import com.wilddeer.escaneodedocumentos.classes.Alumnos;
import com.wilddeer.escaneodedocumentos.classes.ServerURL;
import com.wilddeer.escaneodedocumentos.dialogs.FilterAlumnoDialog;
import com.wilddeer.escaneodedocumentos.utils.AlertDialogCustom;
import com.wilddeer.escaneodedocumentos.utils.Device;
import com.wilddeer.escaneodedocumentos.utils.ViewAnimation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchAlumnosActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private FilterAlumnoDialog dialog;
    private EditText txtBuscar;
    private ImageButton btnLimpiar;
    private ImageButton btnFiltro;
    private ProgressBar progressBar;
    private RecyclerView listAlumnos;
    private LinearLayout lytNoResult;
    private RequestQueue queue;
    private AlumnosAdapter adapter;
    private SwipeRefreshLayout swipe_refresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_alumnos);
        dialog = new FilterAlumnoDialog(this);
        queue = Volley.newRequestQueue(this);
        initToolbar();
        init();

    }

    private void init() {
        findViewById(R.id.btnFiltro).setOnClickListener(v -> dialog.openDialog());
        txtBuscar = findViewById(R.id.txtBuscar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnFiltro = findViewById(R.id.btnFiltro);
        progressBar = findViewById(R.id.progress_bar);
        listAlumnos = findViewById(R.id.listAlumnos);
        lytNoResult = findViewById(R.id.lyt_no_result);
        btnLimpiar.setOnClickListener(click -> txtBuscar.setText(""));
        this.swipe_refresh = findViewById(R.id.swipe_refresh_layout);
        findViewById(R.id.lyt_list).setVisibility(View.GONE);
        swipe_refresh.setOnRefreshListener(this::pullAndRefresh);
        adapter = new AlumnosAdapter(new ArrayList<>(), this);
        adapter.setOnItemClickListener(this::actionItemAlumno);
        listAlumnos.setLayoutManager(new LinearLayoutManager(this));
        listAlumnos.setHasFixedSize(true);
        listAlumnos.setAdapter(adapter);
        txtBuscar.setOnEditorActionListener((v, actionId, event) ->
        {
            if(actionId == 3)
            {
                hideKeyboard();
                consultAlumnos();
                return true;
            }
            return false;
        });
        txtBuscar.setOnKeyListener((v, keyCode, event) -> {

            if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode ==KeyEvent.KEYCODE_ENTER)
            {
                hideKeyboard();
                consultAlumnos();
                return true;
            }
            return false;
        });
    }

    private void pullAndRefresh() {
        swipeProgress(true);
        (new Handler()).postDelayed((Runnable) () -> {
            consultAlumnos();
            swipeProgress(false);
        }, 500L);


    }

    private void swipeProgress(boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(() -> swipe_refresh.setRefreshing(show));
    }

    private void consultAlumnos() {

        if(!Device.isInternetConnection(this))
        {
            AlertDialogCustom.showMensaggeDialog(this, "Error", getString(R.string.conexion_internet));
            return;
        }
        showLoading();

        StringRequest request = new StringRequest(Request.Method.POST,
                ServerURL.consultAlumnos,
                this::loadAlumnos,
                respose -> Log.e("Error", respose.toString())){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("busqueda", txtBuscar.getText().toString());
                params.put("carrera", String.valueOf(dialog.SELETED_CARRERA == null ? 0 :
                        dialog.SELETED_CARRERA.getId()));
                params.put("grupo", String.valueOf(dialog.SELECTED_GRUPO));
                return params;
            }
        };
        queue.add(request);
    }

    private void loadAlumnos(String response) {
        Log.e("Response", response);
        try {
            ArrayList<Alumnos> list = new ArrayList<>();
            JSONObject object = new JSONObject(response);
            JSONArray array = object.getJSONArray("alumnos");

            if(array.length() == 0)
            {
                showEmpty();
                return;
            }
            for (int x = 0; x < array.length(); x++)
                list.add(new Alumnos(array.getJSONObject(x)));
            adapter.setItems(list);
            adapter.notifyDataSetChanged();
            Log.e("MSG", String.valueOf(adapter.getItemCount()));

            showList();
        } catch (JSONException e) {
            e.printStackTrace();
            showEmpty();
        }

    }

    private void actionItemAlumno(View view, Alumnos alumnos, int i) {
        Log.e("MDG", String.valueOf(i));
        Intent intent = new Intent(this, PerfilAlumnoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(getString(R.string.alumno_index), alumnos);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void showLoading()
    {
        progressBar.setVisibility(View.VISIBLE);
        lytNoResult.setVisibility(View.GONE);
        findViewById(R.id.lyt_list).setVisibility(View.GONE);
    }

    private void showEmpty()
    {
        (new Handler()).postDelayed(() -> {
            progressBar.setVisibility(View.GONE);
            lytNoResult.setVisibility(View.VISIBLE);
        },  2000L);
    }

    private void showList(){
        (new Handler()).postDelayed(() -> {
            progressBar.setVisibility(View.GONE);
            findViewById(R.id.lyt_list).setVisibility(View.VISIBLE);
        },  2000L);
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null)
            ((InputMethodManager)getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void searchAction() {
        progressBar.setVisibility(View.VISIBLE);
        this.lytNoResult.setVisibility(View.GONE);

            (new Handler()).postDelayed(() -> {
                progressBar.setVisibility(View.GONE);
                lytNoResult.setVisibility(View.VISIBLE);
            },  2000L);

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        if (paramMenuItem.getItemId() == 16908332) {
            finish();
        }
        return super.onOptionsItemSelected(paramMenuItem);
    }

    private void initToolbar() {
        this.toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Tools.setSystemBarColor(this, 2131099861);
        //Tools.setSystemBarLight((Activity)this);
    }


}
