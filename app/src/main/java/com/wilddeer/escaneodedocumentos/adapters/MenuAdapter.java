package com.wilddeer.escaneodedocumentos.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.wilddeer.escaneodedocumentos.R;
import com.wilddeer.escaneodedocumentos.classes.Menu;

import java.util.ArrayList;

public class MenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    ArrayList<Menu> items;

    public MenuAdapter(Context context, ArrayList<Menu> items)
    {
        this.context = context;
        this.items = items;
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OriginalViewHolder(LayoutInflater.from(context).
                inflate(R.layout.item_select_menu, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Menu menu = items.get(position);
        ((OriginalViewHolder) holder).lbTitulo.setText(menu.getTitulo());
        ((OriginalViewHolder) holder).lbDescripcion.setText(menu.getDescripcion());
        ((OriginalViewHolder) holder).imgFondo.setBackgroundResource(menu.getBackground());
        ((OriginalViewHolder) holder).imgIcon.setImageResource(menu.getIcon());
        ((OriginalViewHolder) holder).btn_list.setOnClickListener(menu.getClick());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        ImageView imgFondo;
        ImageView imgIcon;
        TextView lbTitulo;
        TextView lbDescripcion;
        CardView btn_list;
        public OriginalViewHolder(View view) {
            super(view);
            btn_list = view.findViewById(R.id.btn_list);
            imgFondo = view.findViewById(R.id.thumbnail);
            imgIcon = view.findViewById(R.id.imgIconMes);
            lbTitulo = view.findViewById(R.id.lbTitulo);
            lbDescripcion = view.findViewById(R.id.lbDescription);
        }
    }
}
