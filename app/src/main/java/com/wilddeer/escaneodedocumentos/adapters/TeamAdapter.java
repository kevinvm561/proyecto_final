package com.wilddeer.escaneodedocumentos.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wilddeer.escaneodedocumentos.R;
import java.util.ArrayList;

public class TeamAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<Integrant> integrants;

    public TeamAdapter(Context context)
    {
        this.context = context;
        this.integrants = new ArrayList<>();
        this.integrants = this.getData();
    }

    private ArrayList<Integrant> getData()
    {
        ArrayList<Integrant> list = new ArrayList<>();
        list.add(new Integrant(R.drawable.images, "Brandon Arias", "Programador"));
        list.add(new Integrant(R.drawable.images, "Cristal Germain", "Programador"));
        list.add(new Integrant(R.drawable.images, "German Ramirez", "Programador"));
        list.add(new Integrant(R.drawable.images, "Luis Carlos", "Programador"));
        list.add(new Integrant(R.drawable.images, "Carlos Santiago", "Programador"));
        list.add(new Integrant(R.drawable.images, "Kevin Vazquez", "Programador"));
        return list;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OriginalViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.team_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Integrant integrant = this.integrants.get(position);
        ((OriginalViewHolder)holder).lbNombre.setText(integrant.getNombre());
        ((OriginalViewHolder)holder).lbPuesto.setText(integrant.getPuesto());
        ((OriginalViewHolder)holder).imgPhoto.setImageResource(integrant.getImageId());
    }

    @Override
    public int getItemCount() {
        return this.integrants.size();
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgPhoto;
        public TextView lbNombre;
        public TextView lbPuesto;

        public OriginalViewHolder(View view)
        {
            super(view);
            this.imgPhoto = view.findViewById(R.id.imgPhoto);
            this.lbNombre = view.findViewById(R.id.lbNombre);
            this.lbPuesto = view.findViewById(R.id.lbPuesto);
        }
    }


    private class Integrant {
        private int imageId;
        private String nombre;
        private String puesto;

        public Integrant(@DrawableRes int imageId, String nombre, String puesto) {
            this.imageId = imageId;
            this.nombre = nombre;
            this.puesto = puesto;
        }

        public int getImageId() {
            return imageId;
        }

        public void setImageId(int imageId) {
            this.imageId = imageId;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getPuesto() {
            return puesto;
        }

        public void setPuesto(String puesto) {
            this.puesto = puesto;
        }
    }
}
