package com.wilddeer.escaneodedocumentos.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.RadioButton;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;

import com.wilddeer.escaneodedocumentos.classes.TipoDoc;

import java.util.ArrayList;

public class DocAdapter extends ArrayAdapter<TipoDoc> {
    private ArrayList<TipoDoc> items;
    private LayoutInflater inflater;
    private Context context;
    private int layout;
    public DocAdapter(Context context, @LayoutRes int resource, @IdRes int textViewResource,
                      ArrayList<TipoDoc> items)
    {
        super(context, resource, textViewResource, items);
        this.items = items;
        this.context = context;
        this.layout = resource;
    }
    @Override
    public int getCount() {
        return super.getCount();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(layout, null);
        RadioButton radio = view.findViewById(android.R.id.text1);
        radio.setText(items.get(position).getDoc());
        return view;

    }

}
