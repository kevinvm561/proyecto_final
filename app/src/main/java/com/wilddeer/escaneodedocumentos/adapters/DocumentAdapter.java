package com.wilddeer.escaneodedocumentos.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wilddeer.escaneodedocumentos.R;
import com.wilddeer.escaneodedocumentos.classes.Documento;
import com.wilddeer.escaneodedocumentos.classes.TipoDoc;
import com.wilddeer.escaneodedocumentos.utils.ItemAnimation;

import java.util.ArrayList;

public class DocumentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<Documento> list;
    private int lastPosition = -1;
    private boolean on_attach = true;
    private int animation_type = 2;

    public DocumentAdapter(Context context, ArrayList<Documento> list)
    {
        this.context = context;
        this.list = list;

    }

    public void setItems(ArrayList<Documento> docs)
    {
        this.list = docs;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OriginalViewHolder(LayoutInflater.from(context).
                inflate(R.layout.item_file_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        OriginalViewHolder viewHolder = (OriginalViewHolder) holder;
        Documento documento = list.get(position);
        viewHolder.lbTipoDocumento.setText(documento.getTipoDoc());
        viewHolder.lbNombreArchivo.setText(documento.getNombreArchivo());
        setAnimation(viewHolder.itemView, position);
    }
    private void setAnimation(View paramView, int paramInt) {
        if (paramInt > this.lastPosition) {
            byte b;
            if (this.on_attach) {
                b = (byte) paramInt;
            } else {
                b = -1;
            }
            ItemAnimation.animate(paramView, b, this.animation_type);
            this.lastPosition = paramInt;
        }
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    private class OriginalViewHolder extends RecyclerView.ViewHolder{

        TextView lbTipoDocumento;
        TextView lbNombreArchivo;
        LinearLayout lyt;

        public OriginalViewHolder(View view)
        {
            super(view);
            lyt = view.findViewById(R.id.lyt_parent);
            lbTipoDocumento = view.findViewById(R.id.lbTipoDocumento);
            lbNombreArchivo = view.findViewById(R.id.lbNombreArchivo);
        }
    }

    public Documento isSimiliarDocument(int tipoDoc){
        Documento exito = null;
        for(Documento documento: list)
        {
            if(documento.getDoc().getId() == tipoDoc) {
                exito = documento;
                break;
            }
        }
        return exito;
    }
}
