package com.wilddeer.escaneodedocumentos.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.wilddeer.escaneodedocumentos.R;
import com.wilddeer.escaneodedocumentos.classes.Alumnos;

import java.util.ArrayList;

public class AlumnosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Alumnos> items;
    private Context context;
    private OnItemClickListener mOnItemClickListener;
    public AlumnosAdapter(ArrayList<Alumnos> items, Context context) {
        this.items = items;
        this.context = context;
    }

    public void setOnItemClickListener(OnItemClickListener paramOnItemClickListener) {
        this.mOnItemClickListener = paramOnItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OriginalViewHolder(LayoutInflater.from(context).inflate(R.layout.item_alumno_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        OriginalViewHolder holder1 = (OriginalViewHolder) holder;
        Alumnos alumno = items.get(position);
        holder1.lbCarrera.setText(alumno.getCarrera());
        holder1.lbNombre.setText(alumno.getNombre());
        Picasso.get().load(alumno.getUrlImageFoto()).into(holder1.imagenPerfil);
        holder1.lytParent.setOnClickListener(v -> mOnItemClickListener.onItemClick(v,
                items.get(position), position));
    }

    public interface OnItemClickListener {
        void onItemClick(View param1View, Alumnos param1People, int param1Int);
    }

    public void setItems(ArrayList<Alumnos> alumnos)
    {
        this.items = alumnos;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class OriginalViewHolder extends RecyclerView.ViewHolder{

        LinearLayout lytParent;
        ImageView imagenPerfil;
        TextView lbNombre;
        TextView lbCarrera;

        public OriginalViewHolder(@NonNull View itemView) {
            super(itemView);
            lytParent = itemView.findViewById(R.id.lyt_parent);
            imagenPerfil = itemView.findViewById(R.id.imagenPerfil);
            lbNombre = itemView.findViewById(R.id.lbNombre);
            lbCarrera = itemView.findViewById(R.id.lbCarrera);
        }
    }
}
