package com.wilddeer.escaneodedocumentos.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.ListAdapter;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public abstract class AlertDialogCustom {

    public static void showDialogChoise(Context context, String title, String[] items,
                                        DialogInterface.OnClickListener click) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setNegativeButton("Cancelar", (dialog, which) -> dialog.dismiss());
        builder.setCancelable(false);

        builder.setSingleChoiceItems(items, -1, click);
        builder.show();

    }

    public static void showMensaggeDialog(Context context, String title, String menssage) {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
        builder.setTitle(title).setMessage(menssage).setCancelable(false).setPositiveButton("Aceptar",
                (dialog, which) -> dialog.dismiss()).show();
    }

    public static void showMensaggeDialog(Context context, String title, String menssage,
                                          DialogInterface.OnClickListener okButton) {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
        builder.setTitle(title).setMessage(menssage).setCancelable(false).
                setPositiveButton("Aceptar", okButton).show();
    }

    public static void showMensaggeDialogDecision(Context context, String title, String menssage,
                                                  DialogInterface.OnClickListener okButton) {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
        builder.setTitle(title).setMessage(menssage).setCancelable(false).
                setPositiveButton("Aceptar", okButton).
                setNegativeButton("Cancelar", ((dialog, which) -> dialog.dismiss())).
                show();
    }


    public static void showDialogChoise(Context context, String title, ListAdapter adapter,
                                        DialogInterface.OnClickListener click) {

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
        builder.setTitle(title);
        builder.setNegativeButton("Cancelar", (dialog, which) -> dialog.dismiss());
        builder.setCancelable(false);
        builder.setSingleChoiceItems(adapter, -1, click);
        builder.show();
    }

    public static void showDialogChoise(Context context, @StringRes int title, String[] items,
                                        DialogInterface.OnClickListener click) {
        showDialogChoise(context, context.getString(title), items, click);
    }


}
