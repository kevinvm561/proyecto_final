package com.wilddeer.escaneodedocumentos.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;

public class ItemAnimation {
    public static final int BOTTOM_UP = 1;

    private static final long DURATION_IN_BOTTOM_UP = 150L;

    private static final long DURATION_IN_FADE_ID = 500L;

    private static final long DURATION_IN_LEFT_RIGHT = 150L;

    private static final long DURATION_IN_RIGHT_LEFT = 150L;

    public static final int FADE_IN = 2;

    public static final int LEFT_RIGHT = 3;

    public static final int NONE = 0;

    public static final int RIGHT_LEFT = 4;

    public static void animate(View paramView, int paramInt1, int paramInt2) {
        if (paramInt2 != 1) {
            if (paramInt2 != 2) {
                if (paramInt2 != 3) {
                    if (paramInt2 == 4)
                        animateRightLeft(paramView, paramInt1);
                } else {
                    animateLeftRight(paramView, paramInt1);
                }
            } else {
                animateFadeIn(paramView, paramInt1);
            }
        } else {
            animateBottomUp(paramView, paramInt1);
        }
    }

    private static void animateBottomUp(View paramView, int paramInt) {
        boolean bool;
        float f2;
        long l;
        if (paramInt == -1) {
            bool = true;
        } else {
            bool = false;
        }
        float f1 = 800.0F;
        if (bool) {
            f2 = 800.0F;
        } else {
            f2 = 500.0F;
        }
        paramView.setTranslationY(f2);
        paramView.setAlpha(0.0F);
        AnimatorSet animatorSet = new AnimatorSet();
        if (bool) {
            f2 = f1;
        } else {
            f2 = 500.0F;
        }
        ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(paramView, "translationY", new float[] { f2, 0.0F });
        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(paramView, "alpha", new float[] { 1.0F });
        if (bool) {
            l = 0L;
        } else {
            l = (paramInt + 1) * 150L;
        }
        objectAnimator2.setStartDelay(l);
        if (bool) {
            paramInt = 3;
        } else {
            paramInt = 1;
        }
        objectAnimator2.setDuration(paramInt * 150L);
        animatorSet.playTogether(new Animator[] { (Animator)objectAnimator2, (Animator)objectAnimator1 });
        animatorSet.start();
    }

    private static void animateFadeIn(View paramView, int paramInt) {
        boolean bool;
        long l;
        if (paramInt == -1) {
            bool = true;
        } else {
            bool = false;
        }
        paramView.setAlpha(0.0F);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(paramView, "alpha", new float[] { 0.0F, 0.5F, 1.0F });
        ObjectAnimator.ofFloat(paramView, "alpha", new float[] { 0.0F }).start();
        if (bool) {
            l = 250L;
        } else {
            l = (paramInt + 1) * 500L / 3L;
        }
        objectAnimator.setStartDelay(l);
        objectAnimator.setDuration(500L);
        animatorSet.play((Animator)objectAnimator);
        animatorSet.start();
    }

    private static void animateLeftRight(View paramView, int paramInt) {
        boolean bool;
        long l;
        if (paramInt == -1) {
            bool = true;
        } else {
            bool = false;
        }
        paramView.setTranslationX(-400.0F);
        paramView.setAlpha(0.0F);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(paramView, "translationX", new float[] { -400.0F, 0.0F });
        ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(paramView, "alpha", new float[] { 1.0F });
        ObjectAnimator.ofFloat(paramView, "alpha", new float[] { 0.0F }).start();
        if (bool) {
            l = 150L;
        } else {
            l = (paramInt + 1) * 150L;
        }
        objectAnimator1.setStartDelay(l);
        if (bool) {
            paramInt = 2;
        } else {
            paramInt = 1;
        }
        objectAnimator1.setDuration(paramInt * 150L);
        animatorSet.playTogether(new Animator[] { (Animator)objectAnimator1, (Animator)objectAnimator2 });
        animatorSet.start();
    }

    private static void animateRightLeft(View paramView, int paramInt) {
        boolean bool;
        long l;
        if (paramInt == -1) {
            bool = true;
        } else {
            bool = false;
        }
        paramView.setTranslationX(paramView.getX() + 400.0F);
        paramView.setAlpha(0.0F);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(paramView, "translationX", new float[] { paramView.getX() + 400.0F, 0.0F });
        ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(paramView, "alpha", new float[] { 1.0F });
        ObjectAnimator.ofFloat(paramView, "alpha", new float[] { 0.0F }).start();
        if (bool) {
            l = 150L;
        } else {
            l = (paramInt + 1) * 150L;
        }
        objectAnimator1.setStartDelay(l);
        if (bool) {
            paramInt = 2;
        } else {
            paramInt = 1;
        }
        objectAnimator1.setDuration(paramInt * 150L);
        animatorSet.playTogether(new Animator[] { (Animator)objectAnimator1, (Animator)objectAnimator2 });
        animatorSet.start();
    }
}
