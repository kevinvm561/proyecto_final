package com.wilddeer.escaneodedocumentos.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.wilddeer.escaneodedocumentos.classes.Alumnos;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class CreatePDFDocument extends AsyncTask<String, Integer, String> {
    private String imagePath;
    private Context context;
    private ProgressDialog p;
    private Alumnos alumno;
    private String SELECTED_DOCUMENT;
    private String pdfDocumentPath;
    AsynResponse asynResponse = null;
    private int indentation = 0;
    public CreatePDFDocument(Context context, String imagePath, Alumnos alumnos, String SELECTED_DOCUMENT, AsynResponse asynResponse)
    {
        this.context = context;
        this.alumno = alumnos;
        this.SELECTED_DOCUMENT = SELECTED_DOCUMENT;
        this.imagePath = imagePath;
        this.asynResponse = asynResponse;
    }

    public interface AsynResponse {
        void processFinish(String output);
    }




    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        p = new ProgressDialog(context);
        p.setTitle("Creando Documento");
        p.setMessage("Se esta creando por favor espere");
        p.setIndeterminate(false);
        p.setCancelable(false);
        p.show();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        int current = progress[0];
        int total = progress[1];

        float percentage = 100 * (float)current / (float)total;
        p.setProgress((int) percentage);
        // Display your progress here
    }

    @Override
    protected String doInBackground(String... strings) {
        String res = "";
        this.imagePath = strings[0];
        try {
            res = createPDF(strings[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    private String createPDF(String imagePath) throws IOException {
        String res = "";
        Document document = new Document(PageSize.LEGAL);
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        File pdfFile = File.createTempFile(
                NamesFormat.createDocumentNaming(alumno.getMatricula(), SELECTED_DOCUMENT),
                NamesFormat.PDF_EXTENSION, storageDir);


        try {
            FileOutputStream fos = new FileOutputStream(pdfFile.getAbsolutePath());
            PdfWriter writer = PdfWriter.getInstance(document, fos);
            writer.open();
            document.open();

            Image image = Image.getInstance(imagePath);
            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                    - document.rightMargin() - indentation) / image.getWidth()) * 100;
            image.scalePercent(scaler);
            document.add(image);
            document.close();
            writer.close();
            res = pdfFile.getAbsolutePath();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return res;

    }

    private boolean deleteImage()
    {
        return new File(this.imagePath).delete();
    }
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        p.dismiss();
        deleteImage();
        if(!result.matches(""))
        {
            this.pdfDocumentPath = result;
        }
        asynResponse.processFinish(result);

    }

    public String getPdfDocumentPath()
    {
        return this.pdfDocumentPath;
    }
}
