package com.wilddeer.escaneodedocumentos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.wilddeer.escaneodedocumentos.activitys.ListAlumnosActivity;
import com.wilddeer.escaneodedocumentos.activitys.LoginActivity;
import com.wilddeer.escaneodedocumentos.activitys.MenuActivity;
import com.wilddeer.escaneodedocumentos.activitys.PerfilAlumnoActivity;
import com.wilddeer.escaneodedocumentos.activitys.SearchAlumnosActivity;
import com.wilddeer.escaneodedocumentos.database.DBConfig;
import com.wilddeer.escaneodedocumentos.database.DBUsuarios;
import com.wilddeer.escaneodedocumentos.utils.ViewAnimation;

public class MainActivity extends AppCompatActivity {


    private boolean isLogin = false;
    private int themeOption = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.lyt_logoWild).setVisibility(View.GONE);
        DBUsuarios usuarios = new DBUsuarios(this);
        DBConfig config = new DBConfig(this);
        themeOption = config.getConfig().getThemeOption();
        if(themeOption == 0)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        else
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        (new Handler()).postDelayed(() -> ViewAnimation.showIn(findViewById(R.id.lyt_logoWild)),  400L);

        if(usuarios.consultUsuario() == null) {

            (new Handler()).postDelayed(() -> {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }, 1700L);

        }
        else{
            (new Handler()).postDelayed(() -> {
                Intent intent = new Intent(MainActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }, 1700L);

        }



    }
}
