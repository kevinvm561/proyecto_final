package com.wilddeer.escaneodedocumentos.classes;

import android.content.Context;
import android.content.Intent;
import android.view.View;

public class Menu {
    private String titulo;
    private String descripcion;
    private int icon;
    private int background;
    private View.OnClickListener click;

    public Menu(String titulo, String descripcion, int icon, int background, View.OnClickListener click) {
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.icon = icon;
        this.background = background;
        this.click = click;

    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public void setClick(View.OnClickListener click)
    {
        this.click = click;
    }

    public View.OnClickListener getClick() {
        return click;
    }
}
