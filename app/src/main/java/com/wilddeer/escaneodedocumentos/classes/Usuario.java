/*
 * Copyright (c) 2020. Carlos Santiago, no usar con fines comerciales, solo con fines educativos
 */

package com.wilddeer.escaneodedocumentos.classes;

import org.json.JSONException;
import org.json.JSONObject;

public class Usuario {
    private int id;
    private String user;
    private String pass;
    private String correo;
    private String nombre;

    public Usuario(int id, String user, String pass, String correo, String nombre) {
        this.id = id;
        this.user = user;
        this.pass = pass;
        this.correo = correo;
        this.nombre = nombre;
    }

    public Usuario()
    {

    }

    public Usuario(JSONObject object)
    {
        try
        {
            setId(object.getInt("id"));
            setCorreo(object.getString("correo"));
            setNombre(object.getString("nombre"));
            setUser(object.getString("user"));
            setPass("");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
