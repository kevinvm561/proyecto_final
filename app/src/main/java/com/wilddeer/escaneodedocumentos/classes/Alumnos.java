package com.wilddeer.escaneodedocumentos.classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Alumnos implements Serializable {
    public long id;
    private String matricula;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private ArrayList<Documento> listDocuments;
    private String urlImageFoto;
    private String carrera;
    private String grupo;



    public Alumnos(long id, String matricula, String nombre, String apellidoPaterno, String apellidoMaterno, String urlImageFoto) {
        this.id = id;
        this.matricula = matricula;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.urlImageFoto = urlImageFoto;
    }

    public Alumnos(JSONObject object)
    {
        try
        {
            this.setId(object.getInt("id"));
            this.setNombre(object.getString("nombre"));
            this.setMatricula(object.getString("matricula"));
            this.setApellidoPaterno(object.getString("apellidop"));
            this.setApellidoMaterno(object.getString("apellidom"));
            this.setGrupo(object.getString("grupo"));
            this.setCarrera(object.getString("carrera"));
            this.setUrlImageFoto(object.getString("foto"));
        }catch (JSONException ex)
        {
            ex.printStackTrace();
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public String getUrlImageFoto() {
        return urlImageFoto;
    }

    public void setUrlImageFoto(String urlImageFoto) {
        this.urlImageFoto = urlImageFoto;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public ArrayList<Documento> getListDocuments() {
        return listDocuments;
    }

    public void setListDocuments(ArrayList<Documento> listDocuments) {
        this.listDocuments = listDocuments;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }
}
