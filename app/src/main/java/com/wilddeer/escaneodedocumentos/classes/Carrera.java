package com.wilddeer.escaneodedocumentos.classes;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Carrera {


    private int id;
    private String nombre;
    private ArrayList<Grupo> grupos;
    public final Grupo TODOS_DEFAULT = new Grupo(0, "Todos");
    public Carrera(int id, String nombre, ArrayList<Grupo> grupos) {
        this.id = id;
        this.nombre = nombre;
        this.grupos = grupos;
    }

    public Carrera(JSONObject object)
    {
        grupos = new ArrayList<>();
        grupos.add(TODOS_DEFAULT);
        try
        {

            id = object.getInt("id");
            nombre = object.getString("carrera");
            for (int x = 0; x < object.getJSONArray("grupos").length(); x++)
            {
                grupos.add(new Grupo(object.getJSONArray("grupos").getJSONObject(x)));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Grupo> getGrupos() {
        return grupos;
    }

    public void setGrupos(ArrayList<Grupo> grupos) {
        this.grupos = grupos;
    }

    @NonNull
    @Override
    public String toString() {
        return this.nombre;
    }
}
