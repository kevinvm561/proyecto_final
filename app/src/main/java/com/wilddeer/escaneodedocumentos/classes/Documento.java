package com.wilddeer.escaneodedocumentos.classes;

import org.json.JSONException;
import org.json.JSONObject;

public class Documento {
    private int id;
    private String tipoDoc;
    private String nombreArchivo;
    private String path;
    private TipoDoc doc;


    public Documento(int id, String tipoDoc, String nombreArchivo, String path) {
        this.id = id;
        this.tipoDoc = tipoDoc;
        this.nombreArchivo = nombreArchivo;
        this.path = path;
    }

    public Documento(JSONObject object)
    {
        try{
            this.setId(object.getInt("id"));
            this.setTipoDoc(object.getString("documento"));
            this.setNombreArchivo(object.getString("archivo"));
            this.doc = new TipoDoc(object.getInt("tipoDoc"), object.getString("documento"));
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public TipoDoc getDoc() {
        return doc;
    }

    public void setDoc(TipoDoc doc) {
        this.doc = doc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
