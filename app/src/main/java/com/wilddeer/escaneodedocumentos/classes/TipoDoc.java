package com.wilddeer.escaneodedocumentos.classes;

import org.json.JSONException;
import org.json.JSONObject;

public class TipoDoc {
    private int id;
    private String doc;

    public TipoDoc(int id, String doc) {
        this.id = id;
        this.doc = doc;
    }

    public TipoDoc()
    {

    }

    public TipoDoc(JSONObject object)
    {
        try
        {
            this.id = object.getInt("id");
            this.doc = object.getString("tipo");
        }catch (JSONException ex)
        {
            ex.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public String toString()
    {
        return this.doc;
    }
}
