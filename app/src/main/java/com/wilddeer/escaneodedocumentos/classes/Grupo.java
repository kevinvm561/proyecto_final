package com.wilddeer.escaneodedocumentos.classes;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

public class Grupo {

    private int id;
    private String grupoNombre;


    public Grupo()
    {

    }

    public Grupo(int id, String grupoNombre) {
        this.id = id;
        this.grupoNombre = grupoNombre;
    }

    public Grupo(JSONObject object) {
        try {
            this.id = object.getInt("id");
            this.grupoNombre = object.getString("nombre");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGrupoNombre() {
        return grupoNombre;
    }

    public void setGrupoNombre(String grupoNombre) {
        this.grupoNombre = grupoNombre;
    }

    @NonNull
    @Override
    public String toString() {
        return this.grupoNombre;
    }
}
