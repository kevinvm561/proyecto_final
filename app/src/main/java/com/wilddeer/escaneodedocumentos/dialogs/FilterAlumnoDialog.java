package com.wilddeer.escaneodedocumentos.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.widget.AppCompatSpinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wilddeer.escaneodedocumentos.R;
import com.wilddeer.escaneodedocumentos.classes.Carrera;
import com.wilddeer.escaneodedocumentos.classes.Grupo;
import com.wilddeer.escaneodedocumentos.classes.ServerURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FilterAlumnoDialog {

    private Context context;
    private Dialog dialog;
    private RequestQueue queue;
    private AppCompatSpinner spnGrupos;
    private AppCompatSpinner spnCarreras;
    private ArrayList<Carrera> carrerasList = new ArrayList<>();
    private ArrayAdapter<Carrera> carrerasAdapter;
    public Carrera SELETED_CARRERA;
    public int SELECTED_GRUPO = 0;

    public FilterAlumnoDialog(Context context)
    {
        this.context = context;
        queue = Volley.newRequestQueue(this.context);
        consultCarreras();
        loadDialog();
    }

    private void loadDialog()
    {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_filer_layout);
        dialog.setCancelable(false);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = -1;
        layoutParams.height = -2;
        spnGrupos = dialog.findViewById(R.id.spnGrupos);
        spnCarreras = dialog.findViewById(R.id.spnCarreras);
        spnCarreras.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<Grupo> grupos = ((Carrera) parent.getAdapter().getItem(position)).getGrupos();
                ArrayAdapter<Grupo> adapter = new ArrayAdapter<>(context, R.layout.simple_spiner_item, grupos);
                spnGrupos.setAdapter(adapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnGrupos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                SELECTED_GRUPO = ((Grupo) parent.getAdapter().getItem(position)).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        dialog.findViewById(R.id.btnLimpiar).setOnClickListener(click -> {
            SELETED_CARRERA = null;
            SELECTED_GRUPO = 0;
            dialog.dismiss();
        });
        dialog.findViewById(R.id.btnGuardar).setOnClickListener(this::btnGuardarAction);
        dialog.findViewById(R.id.btnCerrar).setOnClickListener(v -> dialog.dismiss());
        dialog.getWindow().setAttributes(layoutParams);

    }

    private void btnGuardarAction(View view) {
        if(spnCarreras.getSelectedItemPosition() == -1)
        {
            dialog.dismiss();
            return;
        }
        SELETED_CARRERA = (Carrera) spnCarreras.getSelectedItem();
        if(spnGrupos.getSelectedItemPosition() != -1){

        }
        dialog.dismiss();
    }


    private void consultCarreras()
    {
        StringRequest request = new StringRequest(Request.Method.POST, ServerURL.CONSULT_CARRERAS,
                this::loadCarreras, error -> Log.e("ERROR", error.getMessage()));
        queue.add(request);
    }

    private void loadCarreras(String s) {
        try
        {
            JSONObject object = new JSONObject(s);
            JSONArray careras = object.getJSONArray("carreras");
            for (int x = 0; x < careras.length(); x++){
                Carrera car = new Carrera(careras.getJSONObject(x));
                carrerasList.add(car);
            }
            carrerasAdapter = new ArrayAdapter<>(context, R.layout.simple_spiner_item, carrerasList);
            spnCarreras.setAdapter(carrerasAdapter);
            spnGrupos.setSelection(-1);
            spnCarreras.setSelection(-1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void openDialog()
    {
        dialog.show();
    }




}
