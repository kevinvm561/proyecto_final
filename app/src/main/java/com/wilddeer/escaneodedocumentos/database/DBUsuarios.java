package com.wilddeer.escaneodedocumentos.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.wilddeer.escaneodedocumentos.classes.Usuario;

public class DBUsuarios {
    private Context context;
    private DBHelper helper;
    private SQLiteDatabase db;

    private final int DEFAULT_ID = 1;

    private final String COLUMNS[] = {Tables.Usuario.ID_USUARIO, Tables.Usuario.USER,
            Tables.Usuario.NOMBRE, Tables.Usuario.CORREO};
    public DBUsuarios(Context context)
    {
        this.context = context;
        this.helper = new DBHelper(context);

    }

    public long insertUsuario(Usuario usuario)
    {
        ContentValues values = new ContentValues();
        values.put(Tables.Usuario.ID, 1);
        values.put(Tables.Usuario.ID_USUARIO, usuario.getId());
        values.put(Tables.Usuario.USER, usuario.getUser());
        values.put(Tables.Usuario.NOMBRE, usuario.getNombre());
        values.put(Tables.Usuario.CORREO, usuario.getCorreo());
        return db.insert(Tables.Usuario.TABLE_NAME, null, values);
    }

    private Usuario read(Cursor cursor)
    {
        Usuario contacto = new Usuario();
        contacto.setId(cursor.getInt(0));
        contacto.setUser(cursor.getString(1));
        contacto.setNombre(cursor.getString(2));
        contacto.setCorreo(cursor.getString(3));
        return contacto;

    }

    public int deleteUser()
    {
        openDatabase();
        int result = db.delete(Tables.Usuario.TABLE_NAME, "id = ?", new String[]{String.valueOf(DEFAULT_ID)});
        close();
        return result;
    }


    public Usuario consultUsuario()
    {
        Usuario usuario = new Usuario();
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(Tables.Usuario.TABLE_NAME,COLUMNS,
                Tables.Usuario.ID + " = ?" , new String[]{String.valueOf(DEFAULT_ID)},null,
                null, null);
        if(!cursor.moveToFirst()){
            return null;
        }
        usuario =this.read(cursor);
        cursor.close();
        return usuario;


    }

    public void close()
    {
        helper.close();
    }

    public void openDatabase()
    {
        db = helper.getWritableDatabase();
    }


}
