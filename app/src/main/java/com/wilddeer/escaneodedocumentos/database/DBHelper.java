package com.wilddeer.escaneodedocumentos.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.common.collect.Table;

public class DBHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 2;
    private static final String NAME_db = "escaneo.db";
    private static String SQL_DELETE_CONTACTO = "DROP TABLE IF EXISTS " + Tables.Usuario.TABLE_NAME;
    public DBHelper(Context context)
    {
        super(context, NAME_db, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(this.createTableUsuario());
        db.execSQL(this.createTableConfig());
        db.insert(Tables.Config.TABLE_NAME, null, getDefaultConfig());
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_CONTACTO);
        onCreate(db);

    }
    private String createTableConfig()
    {
        SQLiteTable table = new SQLiteTable(Tables.Config.TABLE_NAME, Tables.Config.ID);
        table.addColumn(Tables.Config.THEME, table.TYPE_INTEGER);
        table.addColumn(Tables.Config.URL_CONNECTION, table.TYPE_TEXT);
        return table.getQuery();
    }
    private ContentValues getDefaultConfig()
    {
        ContentValues values = new ContentValues();
        values.put(Tables.Config.ID, 1);
        values.put(Tables.Config.THEME, 1);
        values.put(Tables.Config.URL_CONNECTION, "http://149.56.108.229/documentos/");
        return values;
    }

    private String createTableUsuario()
    {
        SQLiteTable tableContacto = new SQLiteTable(Tables.Usuario.TABLE_NAME, Tables.Usuario.ID);
        tableContacto.addColumn(Tables.Usuario.NOMBRE, tableContacto.TYPE_TEXT);
        tableContacto.addColumn(Tables.Usuario.CORREO, tableContacto.TYPE_TEXT);
        tableContacto.addColumn(Tables.Usuario.PASS, tableContacto.TYPE_TEXT);
        tableContacto.addColumn(Tables.Usuario.USER, tableContacto.TYPE_TEXT);
        tableContacto.addColumn(Tables.Usuario.ID_USUARIO, tableContacto.TYPE_INTEGER);

        return tableContacto.getQuery();
    }




}
