package com.wilddeer.escaneodedocumentos.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.wilddeer.escaneodedocumentos.classes.Config;

public class DBConfig {
    private Context context;
    private DBHelper helper;
    private SQLiteDatabase db;
    private final String COLUMNS[] = {Tables.Config.ID, Tables.Config.THEME,
            Tables.Config.URL_CONNECTION};
    private final int DEFAULT_ID = 1;

    public DBConfig(Context context)
    {
        this.context = context;
        this.helper = new DBHelper(this.context);
    }

    public Config getConfig()
    {
        Config config = new Config();
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(Tables.Config.TABLE_NAME,COLUMNS,
                Tables.Config.ID + " = ?" , new String[]{String.valueOf(DEFAULT_ID)},null,
                null, null);
        if(!cursor.moveToFirst()){
            return null;
        }
        config =this.read(cursor);
        cursor.close();
        return config;
    }

    public long setThemeOption(int option)
    {
        openDatabase();
        ContentValues values = new ContentValues();
        values.put(Tables.Config.THEME, option);

        long result = db.update(Tables.Config.TABLE_NAME, values, "id = ?", new String[]{String.valueOf(DEFAULT_ID)});
        close();
        return result;
    }

    private Config read(Cursor cursor)
    {
        Config contacto = new Config();
        contacto.setThemeOption(cursor.getInt(1));
        contacto.setConnectionServer(cursor.getString(2));

        return contacto;

    }

    public void close()
    {
        helper.close();
    }

    public void openDatabase()
    {
        db = helper.getWritableDatabase();
    }
}
