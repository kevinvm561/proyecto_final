package com.wilddeer.escaneodedocumentos.database;

public abstract class Tables {

    public static class Usuario{
        public static final String TABLE_NAME = "usuarios";
        public static final String ID = "id";
        public static final String ID_USUARIO = "idusuario";
        public static final String USER = "nombre";
        public static final String PASS = "pass";
        public static final String NOMBRE = "nombre";
        public static final String CORREO = "correo";

    }

    public static class Config{
        public static final String TABLE_NAME = "config";

        public static final String ID = "id";
        public static final String THEME = "theme";
        public static final String URL_CONNECTION = "urlconnection";

    }


}
